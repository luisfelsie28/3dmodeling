Mover objeto: g
	Ejes: x y z

Escalar (solo funciona en objetos como cuadrados o circulos): s
	Ejes: x y z

Rellenar: F
	Ejes: x y z

Extruir (aumentar de tamano una cara de un objeto tridimensional): E 
	Ejes: x y z

Duplicar objeto: shift + d 
		 Esc 
		 P 
		 Seleccionar (selection)

Crear corte por la mitad: ctrl + r 
			  con la rueda del mouse se aumenta el numero de cortes 
			  Despues Esc para dejar de modificar 

Crear copias que se modifican cuando modifico la original :  Alt + D


		